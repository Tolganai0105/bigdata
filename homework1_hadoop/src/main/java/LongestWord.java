import java.io.IOException;

import java.util.StringTokenizer;

import java.util.ArrayList;

import java.util.regex.Pattern;

import org.apache.hadoop.conf.Configuration;

import org.apache.hadoop.fs.Path;

import org.apache.hadoop.io.NullWritable;

import org.apache.hadoop.io.Text;

import org.apache.hadoop.mapreduce.*;

import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;

import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;


enum Counters {

    TotalWords,         /** Cчетчик общего количества обработанных слов*/
    MalformedWords      /** Счетчик количества обработанных слов, которые не являются допустимыми словами ASCII**/

}

/**
 Основной класс приложения hadoop
 Программа, которая ищет самое длинное слово в txt-файле
 */

class LongestWord {

    /**
     Класс Mapper
     Разбивает введенный текст на слова, отфильтровывает искаженные слова и map в качестве ключей, чтобы помочь редуктору избежать дубликатов
     */

    public static class TokenizerMapper extends Mapper<Object, Text, Text, NullWritable>

    {

        /**
         Символы, используемые в качестве разделителей, включает в себя общие символы пунктуации
         */

        private static final String delimiters = " !\"#$%&()*+,-./:;<=>@[\\]^_~\n\t\r";

        /**
         Шаблон регулярных выражений для допустимых слов
         Применяется после разделения текста разделителями для обнаружения искаженных слов
         */

        private static final Pattern allowedPattern = Pattern.compile("^[a-zA-Z0-9'`]++$");

        /**

         проверка

         * @param word Слово для проверки валидности

         * @return True, если слово хорошо сформировано, false  в противном случае

         */

        static boolean allowed(String word)

        {

            return allowedPattern.matcher(word).matches();

        }

        /**

         Контейнер-Счетчик

         */

        private final ArrayList<Text> tokenized = new ArrayList<>();

        private int malformed = 0;

        /**

         Разбивает предоставленный текст разделителями, проверяет правильность каждого слова и накапливает хорошо сформированные слова
         * @param value Входной текст

         */

        void tokenize(Text value)

        {


            StringTokenizer itr = new StringTokenizer(value.toString(), delimiters);

            while (itr.hasMoreTokens())

            {

                String str = itr.nextToken();


                if(allowed(str))

                {


                    Text word = new Text(str);

                    tokenized.add(word);

                }

                else

                {

                    ++malformed;

                }

            }

        }

        /**

         Получить накопленные слова

         */

        ArrayList<Text> getTokenized()

        {

            return tokenized;

        }

        /**

         kоличество обнаруженных искаженных слов

         */

        int getMalformed()

        {

            return malformed;

        }

        /**

         Метод Map для процесса MapReduce

         * @param key ключевое значение входного сигнала MapReduce,

         * @param value Входной текст

         */

        @Override

        public void map(Object key, Text value, Context context)

        {

            tokenize(value);

        }

        /**

         * Метод очистки для процесса MapReduce, вызываемый на каждом узле сопоставления после выполнения шага

         * @param context

         * @throws IOException

         * @throws InterruptedException

         */

        @Override

        public void cleanup(Context context) throws IOException, InterruptedException

        {

            for(Text val : tokenized)

            {

                context.write(val, NullWritable.get());

            }

            context.getCounter(Counters.TotalWords).increment(tokenized.size() + malformed);

            context.getCounter(Counters.MalformedWords).increment(malformed);

        }

    }

    /**

     Класс Reducer

     Находит самое длинное слово во входной последовательности слов и удаляет повторяющиеся слова

     */

    public static class FilterReducer extends Reducer<Text,NullWritable,Text,NullWritable>

    {

        /**

         максимальная длина слова

         */

        private int max = Integer.MIN_VALUE;

        /**

         * Контейнер накопленных слов

         */

        private final ArrayList<Text> words = new ArrayList<>();


        void processWord(Text key)

        {

            int len = key.getLength();

            if(len > max)

            {


                max = len;

                words.clear();

            }

            if(len == max)

            {


                words.add(new Text(key));

            }

        }


        ArrayList<Text> getWords()

        {

            return words;

        }

        /**

         * Метод Reduce для процесса MapReduce

         */

        @Override

        public void reduce(Text key, Iterable<NullWritable> value, Context context)

        {

            processWord(key);

        }

        /**

         * Метод очистки для процесса MapReduce

         */

        @Override

        public void cleanup(Context context) throws IOException, InterruptedException

        {

            for(Text val : words)

            {

                context.write(val, NullWritable.get());

            }

        }

    }

    /**

     * MapReduce

     */

    public static void main(String[] args) throws Exception

    {

        Configuration conf = new Configuration();

        Job job = Job.getInstance(conf, "longest word");

        job.setJarByClass(LongestWord.class);

        job.setMapperClass(TokenizerMapper.class);

        job.setCombinerClass(FilterReducer.class);

        job.setReducerClass(FilterReducer.class);

        job.setOutputKeyClass(Text.class);

        job.setOutputValueClass(NullWritable.class);

        FileInputFormat.addInputPath(job, new Path(args[0]));

        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        if(job.waitForCompletion(true))

        {

            System.out.println("Total amount of words : " + job.getCounters().findCounter(Counters.TotalWords).getValue());

            System.out.println("Amount of malformed words : " + job.getCounters().findCounter(Counters.MalformedWords).getValue());

            System.exit(0);

        }

        System.exit(1);

    }

}