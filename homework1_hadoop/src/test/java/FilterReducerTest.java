import org.apache.commons.lang.RandomStringUtils;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.ThreadLocalRandom;

import static org.junit.Assert.*;

public class FilterReducerTest
{
    private LongestWord.FilterReducer reducer;

    @Before
    public void initTest() {
        reducer = new LongestWord.FilterReducer();
    }

    @After
    public void afterTest() {
        reducer = null;
    }

    @Test
    public void testSingleWord()
    {
        int size = ThreadLocalRandom.current().nextInt(1024) + 1;
        String word = RandomStringUtils.randomAlphanumeric(size);
        reducer.processWord(new Text(word));
        assertEquals(1, reducer.getWords().size());
        assertEquals(word, reducer.getWords().get(0).toString());
    }

    @Test
    public void testManyWordsOfSameSize()
    {
        int size = ThreadLocalRandom.current().nextInt(1024) + 1;
        int wcount = ThreadLocalRandom.current().nextInt(1024) + 2;
        ArrayList<String> words = new ArrayList<>();
        for(int i = 0 ; i < wcount ; ++i)
        {
            words.add(RandomStringUtils.randomAlphanumeric(size));
        }
        for(String i : words)
        {
            Vector<IntWritable> vec = new Vector<>();
            int sizenum = ThreadLocalRandom.current().nextInt(1024) + 2;
            for(int j = 0 ; j < sizenum ; ++j)
            {
                vec.add(new IntWritable(size));
            }
            reducer.processWord(new Text(i));
        }
        assertEquals(wcount, reducer.getWords().size());
        for(int i = 0 ; i < wcount ; ++i)
        {
            assertEquals(words.get(i), reducer.getWords().get(i).toString());
        }
    }

    @Test
    public void testManyWordsOfRandomSize()
    {
        int maxSize = 0;
        int wcount = ThreadLocalRandom.current().nextInt(1024) + 2;
        ArrayList<String> words = new ArrayList<>();
        for(int i = 0 ; i < wcount ; ++i)
        {
            int size = ThreadLocalRandom.current().nextInt(1024) + 1;
            if(size > maxSize)
            {
                maxSize = size;
            }
            words.add(RandomStringUtils.randomAlphanumeric(size));
        }
        for(String i : words)
        {
            reducer.processWord(new Text(i));
        }
        ArrayList<String> maxwords = new ArrayList<>();
        for(String i : words)
        {
            if(i.length() == maxSize)
            {
                maxwords.add(i);
            }
        }
        assertEquals(maxwords.size(), reducer.getWords().size());
        Text text = new Text();
        for(String i : maxwords)
        {
            text.set(i);
            assertTrue(reducer.getWords().indexOf(text) >= 0);
        }
    }

    @Test
    public void testWithDriver() throws IOException
    {
        int maxSize = 0;
        int wcount = ThreadLocalRandom.current().nextInt(1024) + 2;
        ArrayList<String> words = new ArrayList<>();
        for(int i = 0 ; i < wcount ; ++i)
        {
            int size = ThreadLocalRandom.current().nextInt(1024) + 1;
            if(size > maxSize)
            {
                maxSize = size;
            }
            words.add(RandomStringUtils.randomAlphanumeric(size));
        }
        ArrayList<String> maxwords = new ArrayList<>();
        for(String i : words)
        {
            if(i.length() == maxSize)
            {
                maxwords.add(i);
            }
        }

        ReduceDriver<Text, NullWritable, Text, NullWritable> driver = ReduceDriver.newReduceDriver(reducer);
        List<NullWritable> list = new Vector<>();
        list.add(NullWritable.get());
        for(String i : words)
        {
            driver.withInput(new Text(i), list);
        }
        for(String i : maxwords)
        {
            driver.withOutput(new Text(i), NullWritable.get());
        }
        driver.runTest();
    }
}
