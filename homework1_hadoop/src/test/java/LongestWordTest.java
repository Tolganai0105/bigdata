import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;
import java.util.concurrent.ThreadLocalRandom;

import static org.junit.Assert.*;

public class LongestWordTest
{

    private LongestWord.TokenizerMapper mapper;
    private LongestWord.FilterReducer reducer;
    private MapReduceDriver<Object, Text, Text, NullWritable, Text, NullWritable> driver;

    private static final String delimers = " !\"#$%&()*+,-./:;<=>@[\\]^_~\n\t\r";

    static final String goodChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'`";

    static final String badChars = getBadChars();

    static private String getBadChars()
    {
        StringBuilder strb = new StringBuilder();
        for(char i = Character.MIN_VALUE; i < Character.MAX_VALUE; ++i)
        {
            if(goodChars.indexOf(i) == -1 && delimers.indexOf(i) == -1)
            {
                strb.append(i);
            }
        }
        if(goodChars.indexOf(Character.MAX_VALUE) == -1 && delimers.indexOf(Character.MAX_VALUE) == -1)
        {
            strb.append(Character.MAX_VALUE);
        }
        return strb.toString();
    }

    static private String getGoodString()
    {
        int size = ThreadLocalRandom.current().nextInt(1024) + 1;
        StringBuilder strb = new StringBuilder(size);
        for(int i = 0 ; i < size ; ++i)
        {
            int pos = ThreadLocalRandom.current().nextInt(goodChars.length());
            strb.append(goodChars.charAt(pos));
        }
        return strb.toString();
    }

    static private Vector<Integer> getRandomPositions(int size)
    {
        Vector<Integer> ids = new Vector<>(size);
        for(int i = 0 ; i < size ; ++i)
        {
            ids.add(i);
        }
        for(int i = 0 ; i < size ; ++i)
        {
            int index = ThreadLocalRandom.current().nextInt(ids.size());
            Integer swp = ids.get(i);
            ids.set(i, ids.get(index));
            ids.set(index, swp);
        }
        int changeNum = ThreadLocalRandom.current().nextInt(size) + 1;
        ids.setSize(changeNum);
        return ids;
    }

    static private String getBadString()
    {
        StringBuilder strb = new StringBuilder(getGoodString());
        Vector<Integer> ids = getRandomPositions(strb.length());
        for(Integer pos : ids)
        {
            char ch = badChars.charAt(ThreadLocalRandom.current().nextInt(badChars.length()));
            strb.setCharAt(pos, ch);
        }
        return strb.toString();
    }

    static private String combineStrings(Iterable<String> values)
    {
        StringBuilder stb = new StringBuilder();
        for(String word : values)
        {
            stb.append(word);
            stb.append(delimers.charAt(ThreadLocalRandom.current().nextInt(delimers.length())));

        }
        return stb.toString();
    }

    @Before
    public void initTest()
    {
        mapper = new LongestWord.TokenizerMapper();
        reducer = new LongestWord.FilterReducer();
        driver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
    }

    @After
    public void afterTest() {
        mapper = null;
        reducer = null;
        driver = null;
    }

    @Test
    public void testWithDriver() throws IOException
    {
        int wordCount = ThreadLocalRandom.current().nextInt(1024) + 1;
        Vector<String> words = new Vector<>(wordCount);
        for (int i = 0; i < wordCount; ++i)
        {
            words.add(getGoodString());
        }

        Vector<Integer> badpos = getRandomPositions(wordCount);
        for (Integer i : badpos)
        {
            words.set(i, getBadString());
        }

        String text = combineStrings(words);
        Vector<String> goodWords = new Vector<>();
        int maxSize = 0;
        for (int i = 0; i < words.size(); ++i)
        {
            if ( ! badpos.contains(i))
            {
                goodWords.add(words.get(i));
                if(words.get(i).length() > maxSize)
                {
                    maxSize = words.get(i).length();
                }
            }
        }

        ArrayList<String> maxwords = new ArrayList<>();
        for(String i : goodWords)
        {
            if(i.length() == maxSize)
            {
                maxwords.add(i);
            }
        }

        driver.withInput(NullWritable.get(), new Text(text));
        for(String i : maxwords)
        {
            driver.withOutput(new Text(i), NullWritable.get());
        }
        driver.runTest();
        assertEquals(words.size(), driver.getCounters().findCounter(Counters.TotalWords).getValue());
        assertEquals(badpos.size(), driver.getCounters().findCounter(Counters.MalformedWords).getValue());
    }

}