import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;
import java.util.concurrent.ThreadLocalRandom;

import static org.junit.Assert.*;

public class TokenizerMapperTest
{

    private LongestWord.TokenizerMapper mapper;

    private static final String delimers = " !\"#$%&()*+,-./:;<=>@[\\]^_~\n\t\r";

    static final String goodChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'`";

    static final String badChars = getBadChars();

    static private String getBadChars()
    {
        StringBuilder strb = new StringBuilder();
        for(char i = Character.MIN_VALUE; i < Character.MAX_VALUE; ++i)
        {
            if(goodChars.indexOf(i) == -1 && delimers.indexOf(i) == -1)
            {
                strb.append(i);
            }
        }
        if(goodChars.indexOf(Character.MAX_VALUE) == -1 && delimers.indexOf(Character.MAX_VALUE) == -1)
        {
            strb.append(Character.MAX_VALUE);
        }
        return strb.toString();
    }

    static private String getGoodString()
    {
        int size = ThreadLocalRandom.current().nextInt(1024) + 1;
        StringBuilder strb = new StringBuilder(size);
        for(int i = 0 ; i < size ; ++i)
        {
            int pos = ThreadLocalRandom.current().nextInt(goodChars.length());
            strb.append(goodChars.charAt(pos));
        }
        return strb.toString();
    }

    static private Vector<Integer> getRandomPositions(int size)
    {
        Vector<Integer> ids = new Vector<>(size);
        for(int i = 0 ; i < size ; ++i)
        {
            ids.add(i);
        }
        for(int i = 0 ; i < size ; ++i)
        {
            int index = ThreadLocalRandom.current().nextInt(ids.size());
            Integer swp = ids.get(i);
            ids.set(i, ids.get(index));
            ids.set(index, swp);
        }
        int changeNum = ThreadLocalRandom.current().nextInt(size) + 1;
        ids.setSize(changeNum);
        return ids;
    }

    static private String getBadString()
    {
        StringBuilder strb = new StringBuilder(getGoodString());
        Vector<Integer> ids = getRandomPositions(strb.length());
        for(Integer pos : ids)
        {
            char ch = badChars.charAt(ThreadLocalRandom.current().nextInt(badChars.length()));
            strb.setCharAt(pos, ch);
        }
        return strb.toString();
    }

    static private String combineStrings(Iterable<String> values)
    {
        StringBuilder stb = new StringBuilder();
        for(String word : values)
        {
            stb.append(word);
            stb.append(delimers.charAt(ThreadLocalRandom.current().nextInt(delimers.length())));

        }
        return stb.toString();
    }

    @Before
    public void initTest() {
        mapper = new LongestWord.TokenizerMapper();
    }

    @After
    public void afterTest() {
        mapper = null;
    }

    @Test
    public void testAllowedWord() {
        assertTrue(LongestWord.TokenizerMapper.allowed(getGoodString()));
    }

    @Test
    public void testNotAllowedWord() {
        assertFalse(LongestWord.TokenizerMapper.allowed(getBadString()));
    }

    @Test
    public void testNotAllowedEmptyWord() {
        assertFalse(LongestWord.TokenizerMapper.allowed(""));
    }

    @Test
    public void testTokenizerAllGood() {
        int wordCount = ThreadLocalRandom.current().nextInt(1024) + 1;
        Vector<String> words = new Vector<>(wordCount);
        for(int i = 0; i < wordCount; ++i)
        {
            words.add(getGoodString());
        }
        String text = combineStrings(words);
        mapper.tokenize(new Text(text));
        ArrayList<Text> tokens = mapper.getTokenized();
        assertEquals(words.size(), tokens.size());
        assertEquals(0, mapper.getMalformed());
        for(int i = 0 ; i < words.size() ; ++i)
        {
            assertEquals(words.get(i), tokens.get(i).toString());
        }
    }

    @Test
    public void testTokenizerSomeBad()
    {
        int wordCount = ThreadLocalRandom.current().nextInt(1024) + 1;
        Vector<String> words = new Vector<>(wordCount);
        for (int i = 0; i < wordCount; ++i)
        {
            words.add(getGoodString());
        }

        Vector<Integer> badpos = getRandomPositions(wordCount);
        for (Integer i : badpos)
        {
            words.set(i, getBadString());
        }

        String text = combineStrings(words);
        mapper.tokenize(new Text(text));
        ArrayList<Text> tokens = mapper.getTokenized();
        assertEquals(words.size() - badpos.size(), tokens.size());
        assertEquals(badpos.size(), mapper.getMalformed());
        int wpos = 0;
        for (int i = 0; i < tokens.size(); ++i, ++wpos)
        {
            while (badpos.contains(wpos))
            {
                ++wpos;
            }
            assertEquals(words.get(wpos), tokens.get(i).toString());
        }
    }

    @Test
    public void testWithDriver() throws IOException
    {
        int wordCount = ThreadLocalRandom.current().nextInt(1024) + 1;
        Vector<String> words = new Vector<>(wordCount);
        for (int i = 0; i < wordCount; ++i)
        {
            words.add(getGoodString());
        }

        Vector<Integer> badpos = getRandomPositions(wordCount);
        for (Integer i : badpos)
        {
            words.set(i, getBadString());
        }

        String text = combineStrings(words);
        Vector<String> goodWords = new Vector<>();
        for (int i = 0; i < words.size(); ++i)
        {
            if ( ! badpos.contains(i))
            {
                goodWords.add(words.get(i));
            }
        }
        MapDriver<Object, Text, Text, NullWritable> driver = MapDriver.newMapDriver(mapper);
        driver.withInput(NullWritable.get(), new Text(text));
        for(String i : goodWords)
        {
            driver.withOutput(new Text(i), NullWritable.get());
        }
        driver.runTest();
        assertEquals(words.size(), driver.getCounters().findCounter(Counters.TotalWords).getValue());
        assertEquals(badpos.size(), driver.getCounters().findCounter(Counters.MalformedWords).getValue());
    }

}