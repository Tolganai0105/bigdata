Вариант a(1), b(1), c(3), d(2)
1.	Бизнес-логика:
Программа, которая ищет самое длинное слово в текстовом файле (должны приниматься только символы ASCII) 

2. Выходной формат
CSV

3. Дополнительные условия
Счетчики используются для сбора статистики о неправильно сформированных строках

4.	Отчёт должен включать (обязательно):
•	Папка src в формате ZIP с вашей реализацией
•	Снимок экрана успешно выполненных тестов
•	Снимок экрана успешно загруженного файла в HDFS
•	Снимки экрана успешно выполненного задания и результата
•	Руководство по быстрой сборке и развертыванию (команды, требования к ОС и т. д.)
